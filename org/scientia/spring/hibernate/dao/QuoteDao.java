package org.scientia.spring.hibernate.dao;

import java.util.List;

import org.scientia.spring.hibernate.entity.Quote;
 
//The CRUD

public interface QuoteDao {
    public void saveQuote(Quote quote);
    public List<Quote> getAllQuote(Quote quote);
    public Quote selectQuoteById(Long id);
    public void deleteQuote(Quote quote);
}