package org.scientia.spring.hibernate.dao;

import java.util.List;

import org.scientia.spring.hibernate.entity.Quote;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository("QuoteDao")
@Transactional
public class QuoteDaoImpl implements QuoteDao {
	private HibernateTemplate hibernateTemplate;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Transactional(readOnly = false)
	public void saveQuote(Quote quote) {
		hibernateTemplate.saveOrUpdate(quote);

	}

	@Transactional(readOnly = false)
	public void deleteQuote(Quote quote) {
		hibernateTemplate.delete(quote);

	}

	@SuppressWarnings("unchecked")
	public List<Quote> getAllQuote(Quote quote) {
		return (List<Quote>) hibernateTemplate.find("from " + Quote.class.getName());
	}

	public Quote selectQuoteById(Long id) {
		return hibernateTemplate.get(Quote.class, id);
	}

}

